// Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

let btn = document.getElementsByClassName("btn");

btn[0].addEventListener("click", function () { SubmitPass() });

function SubmitPass() {
    let password = document.getElementById("mainPass").value
    let passwordSub = document.getElementById("submPass").value

    if (password === passwordSub) {
        document.getElementById('error-message').style.display = 'none'
        alert('You are welcome')
    } else {
        document.getElementById('error-message').style.display = 'block'
    }
}

document.addEventListener("DOMContentLoaded", function (){
function ShowHide(item) {
    let showHideButton = item.querySelectorAll('.icon-password')
    for (iter of showHideButton) {
        let input = item.querySelector('.password');
        let eye = item.querySelector('.fa-eye')
        let eyeSlash = item.querySelector('.fa-eye-slash')
        iter.addEventListener('click', function () {
                    if (input.type === 'password') {
                        input.type = 'text';

                        eye.style.visibility = 'hidden'
                        eyeSlash.style.visibility = 'visible'
                    } else {
                        input.type = 'password';
                        eyeSlash.style.visibility = 'hidden'
                        eye.style.visibility = 'visible'
                    }
            }
        )
    }
}

let wrappers = document.getElementsByClassName('input-wrapper')
for ( item of wrappers){
    ShowHide(item)
}
})