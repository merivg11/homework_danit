let activeButton = '';
window.addEventListener('keydown', (event) => {
    let name = event.key;
    const names = ['Enter', 's', 'e', 'o', 'n', 'l', 'z'];
    const buttonId = ['enter-button', 's-button', 'e-button', 'o-button', 'n-button', 'l-button', 'z-button']
    if (names.includes(name)) {
        if (activeButton !== '') {
            document.getElementById(activeButton).style.backgroundColor = "black";
        }
        activeButton = buttonId[names.indexOf(name)];
        document.getElementById(activeButton).style.backgroundColor = "blue";
    }

}, false);
