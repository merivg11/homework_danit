// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.

const input = document.getElementById("input")
const delButton = document.createElement('div')
delButton.id = 'delButton'
let span = document.createElement('span')
span.id = "current-price"

const phrase = document.createElement('p')
phrase.innerHTML = 'Please enter correct price.'
input.after(phrase)
phrase.className = 'phrase-hidden'
input.addEventListener("focus",function(){
    if (document.getElementById('current-price')&&document.getElementById('delButton')){
        span.remove()
        delButton.remove()
    }
    input.className = 'focus'
    phrase.className = 'phrase-hidden'

})

input.addEventListener("focusout", function(){
    input.classList.remove('focus')
    if(input.value < 0){
        input.className = 'invalid'
        phrase.className = 'phrase-visible'

    }else {
        span.className = 'span'
        span.innerHTML = `Текущая цена: ${input.value}`
        input.before(span)
        delButton.className = 'closeButton'
        span.appendChild(delButton)
        input.className = 'valid'
        phrase.className = 'phrase-hidden'
    }
})
delButton.addEventListener( "click", function (){
    span.remove()
    delButton.remove()
    input.value = ''
    }
)
