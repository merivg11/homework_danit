// Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.
// Технические требования:
// Отправить AJAX запрос по адресу https://ajax.test-danit.com/api/swapi/films и получить список всех фильмов серии Звездные войны
// Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства characters.
// Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
// Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.

fetch( 'https://ajax.test-danit.com/api/swapi/films')
.then(response => response.json())
.then(data =>{
    data.forEach(obj => {
        let filmInfo = document.createElement('ul')
        filmInfo.innerHTML = `<li> ${obj.episodeId} </li> <li class="film-name"> ${obj.name} </li> <li> ${obj.openingCrawl} </li>`
        document.body.appendChild(filmInfo)
        let charactersArray = obj.characters
        const promises = charactersArray.map(el => fetch(el).then(response => response.json()))
        Promise.all(promises)
            .then(characters => {
                let charList = document.createElement('ul')
                 characterElement = characters.map(elem => {
                    return character = `<li> ${elem.name} </li>`
                }).join('')
                charList.innerHTML = characterElement
                filmInfo.getElementsByClassName('film-name')[0].after(charList)
            })
    })
})