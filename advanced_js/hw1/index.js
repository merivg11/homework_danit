// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
//     Создайте геттеры и сеттеры для этих свойств.
//     Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.
class Employee{
    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }
    get name(){
        return this._name
    }
    get age(){
        return this._age
    }
    get salary(){
        return this._salary
    }
    set name(name){
        this._name = name
    }
    set age(age){
        this._age = age
    }
    set salary(salary){
        this._salary = salary
    }

}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang
    }
    get salary(){
        return this._salary*3
    }
}


const john = new Programmer('John', 30, 1000, ['eng', 'rus'])
const anne = new Programmer('Anne', 21, 2500, ['eng', 'ukr', 'rus', 'ger'])

console.log(john)
console.log(anne)
console.log(john.salary)
console.log(anne.salary)




