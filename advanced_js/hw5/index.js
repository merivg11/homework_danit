// Создать простую HTML страницу с кнопкой Вычислить по IP.
//     По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
//     Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
//     Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
//     Все запросы на сервер необходимо выполнить с помощью async await.

const infoList = document.createElement('ul')
const button = document.getElementById('button')
button.after(infoList)

async function IpDetect(){
    const responceIp = await fetch('https://api.ipify.org/?format=json')
    const resp = await responceIp.json()
    return resp.ip
}
async function getInfoByIp(ip){
    const responceInfo = (await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`)).json()
    return responceInfo
}
function showInfo(elem){
    const item = document.createElement('li')
    item.innerHTML = elem
    infoList.appendChild(item)
}
button.addEventListener("click", () =>{
    IpDetect().then(data => getInfoByIp(data)).then(data =>{
        const infoItem = Object.values(data)
        infoItem.forEach(elem =>{
            showInfo(elem)
        })
    })
})
