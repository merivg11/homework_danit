// Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
//     Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.




    const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер1",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

//make list of object's properties
function makeUL(element) {
    let entries = Object.entries(element);
    innerArr = entries.map(function (innerArrItem) {
        return '<li>' + innerArrItem[0] + ':' + innerArrItem[1] + '</li>'
    }).join('')
    element = '<ul style="list-style: none">' + innerArr + '</ul>'
    return element
}

// check if all properties are present
function checkProperties (element, currentObjProperties, mandatoryProperties){
    return mandatoryProperties.every(v => currentObjProperties.includes(v))
}

//display elements from given array
    function showElements (array, checkPropFunction){
        let list = document.createElement('ul')
        document.getElementById('root').appendChild(list)
        list.id = 'list'
        const mandatoryProperties = ["author", "name", "price"]
        array.forEach(element =>{
            let currentObjProperties = Object.getOwnPropertyNames(element)
            try {
                if(Boolean(checkPropFunction(element,currentObjProperties,mandatoryProperties)) === false){
                    throw new Error(`нет свойства ${mandatoryProperties.filter(x => !currentObjProperties.includes(x))}`)
                }
            }catch (e){
                console.log(e.message)
                return
            }
            let listItem = document.createElement('li')
            listItem.innerHTML = makeUL(element)
            document.getElementById('list').appendChild(listItem)
        })
    }


showElements (books, checkProperties)


